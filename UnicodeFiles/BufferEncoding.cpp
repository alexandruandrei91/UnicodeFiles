#include "stdafx.h"

#include <algorithm>

#include "BufferEncoding.h"
#include "Utf8_16.h"

using namespace UnicodeFiles;

const std::vector<unsigned char> BufferEncoding::kUtf8_BOM = { 0xEF, 0xBB, 0xBF };
const std::vector<unsigned char> BufferEncoding::kUtf16LE_BOM = { 0xFF, 0xFE };
const std::vector<unsigned char> BufferEncoding::kUtf16BE_BOM = { 0xFE, 0xFF };

CharEncoding BufferEncoding::GetEncoding(const std::vector<unsigned char>& aBuffer)
{
	auto bom = GetBom(aBuffer);

	if (bom.size() == 3)
		return CharEncoding::Utf8;
	
	if (bom.size() == 2)
	{
		if (bom == kUtf16LE_BOM)
			return CharEncoding::Utf16_LE;

		if (bom == kUtf16BE_BOM)
			return CharEncoding::Utf16_BE;
	}

	Utf8_16_Read npp(const_cast<unsigned char*>(aBuffer.data()), aBuffer.size());
	npp.determineEncoding();

	switch (npp.getEncoding())
	{
	case uni7Bit:
	case uni8Bit:
		return CharEncoding::Ansi;
	case uniCookie:
		return CharEncoding::Utf8_NoBom;
	case uni16LE_NoBOM:
		return CharEncoding::Utf16_LE_NoBom;
	case uni16BE_NoBOM:
		return CharEncoding::Utf16_BE_NoBom;
	default:
		return CharEncoding::Unknown_Encoding;
	}
}

std::vector<unsigned char> BufferEncoding::Convert(const std::vector<unsigned char>& aBuffer, CharEncoding aEncoding)
{
	CharEncoding buffEncoding = GetEncoding(aBuffer);

	if (buffEncoding == aEncoding)
		return aBuffer;

	std::vector<unsigned char> buff;

	switch (buffEncoding)
	{
	case CharEncoding::Ansi:
		buff = ConvertFromAnsi(StripBom(aBuffer), aEncoding);
		break;
	case CharEncoding::Utf8:
	case CharEncoding::Utf8_NoBom:
		buff = ConvertFromUtf8(StripBom(aBuffer), aEncoding);
		break;
	case CharEncoding::Utf16_LE:
	case CharEncoding::Utf16_LE_NoBom:
		buff = ConvertFromUtf16LE(StripBom(aBuffer), aEncoding);
		break;
	case CharEncoding::Utf16_BE:
	case CharEncoding::Utf16_BE_NoBom:
		buff = ConvertFromUtf16BE(StripBom(aBuffer), aEncoding);
		break;
	}

	if (aEncoding != CharEncoding::Ansi)
	{
		if (!HasBom(aEncoding))
		{
			buff = StripBom(buff);
		}
		else
		{
			buff = AddBom(buff, aEncoding);
		}
	}

	return buff;
}

bool BufferEncoding::HasBom(CharEncoding aEncoding)
{
	return aEncoding == CharEncoding::Utf8 || aEncoding == CharEncoding::Utf16_LE || aEncoding == CharEncoding::Utf16_BE;
}

bool BufferEncoding::HasBom(const std::vector<unsigned char>& aBuffer)
{
	return GetBom(aBuffer).size() != 0;
}

std::vector<unsigned char> BufferEncoding::StripBom(const std::vector<unsigned char>& aBuffer)
{
	return std::vector<unsigned char>(aBuffer.begin() + GetBom(aBuffer).size(), aBuffer.end());
}

std::vector<unsigned char> BufferEncoding::AddBom(const std::vector<unsigned char>& aBuffer, CharEncoding aEncoding)
{
	if (HasBom(aBuffer) || !HasBom(aEncoding))
		return aBuffer;

	std::vector<unsigned char> buff;
	switch (aEncoding)
	{
	case CharEncoding::Utf8:
		buff = kUtf8_BOM;
		break;
	case CharEncoding::Utf16_LE:
		buff = kUtf16LE_BOM;
		break;
	case CharEncoding::Utf16_BE:
		buff = kUtf16BE_BOM;
		break;
	}
	buff.insert(buff.end(), aBuffer.begin(), aBuffer.end());

	return buff;
}

std::vector<unsigned char> BufferEncoding::GetBom(const std::vector<unsigned char>& aBuffer)
{
	if (aBuffer.size() >= 3 &&
		aBuffer[0] == 0xEF &&
		aBuffer[1] == 0xBB &&
		aBuffer[2] == 0xBF)
		return kUtf8_BOM;

	if (aBuffer.size() >= 2 &&
		aBuffer[0] == 0xFF &&
		aBuffer[1] == 0xFE)
		return kUtf16LE_BOM;

	if (aBuffer.size() >= 2 &&
		aBuffer[0] == 0xFE &&
		aBuffer[1] == 0xFF)
		return kUtf16BE_BOM;

	return{};
}

//-------------------------------------------------------------------------------------------------

std::vector<unsigned char> BufferEncoding::ConvertFromAnsi(const std::vector<unsigned char>& aBuffer, CharEncoding aEncoding)
{
	switch (aEncoding)
	{
	case CharEncoding::Utf8:
	case CharEncoding::Utf8_NoBom:
		return Ansi_Utf8(aBuffer);
	case CharEncoding::Utf16_LE:
	case CharEncoding::Utf16_LE_NoBom:
		return Ansi_Utf16LE(aBuffer);
	case CharEncoding::Utf16_BE:
	case CharEncoding::Utf16_BE_NoBom:
		return Ansi_Utf16BE(aBuffer);
	default:
		return aBuffer;
	}
}

std::vector<unsigned char> BufferEncoding::ConvertFromUtf8(const std::vector<unsigned char>& aBuffer, CharEncoding aEncoding)
{
	switch (aEncoding)
	{
	case CharEncoding::Ansi:
		return Utf8_Ansi(aBuffer);
	case CharEncoding::Utf16_LE:
	case CharEncoding::Utf16_LE_NoBom:
		return Utf8_Utf16LE(aBuffer);
	case CharEncoding::Utf16_BE:
	case CharEncoding::Utf16_BE_NoBom:
		return Utf8_Utf16BE(aBuffer);
	default:
		return aBuffer;
	}
}

std::vector<unsigned char> BufferEncoding::ConvertFromUtf16LE(const std::vector<unsigned char>& aBuffer, CharEncoding aEncoding)
{
	switch (aEncoding)
	{
	case CharEncoding::Ansi:
		return Utf16LE_Ansi(aBuffer);
	case CharEncoding::Utf8:
	case CharEncoding::Utf8_NoBom:
		return Utf16LE_Utf8(aBuffer);
	case CharEncoding::Utf16_BE:
	case CharEncoding::Utf16_BE_NoBom:
		return Utf16LE_Utf16BE(aBuffer);
	default:
		return aBuffer;
	}
}

std::vector<unsigned char> BufferEncoding::ConvertFromUtf16BE(const std::vector<unsigned char>& aBuffer, CharEncoding aEncoding)
{
	switch (aEncoding)
	{
	case CharEncoding::Ansi:
		return Utf16BE_Ansi(aBuffer);
	case CharEncoding::Utf8:
	case CharEncoding::Utf8_NoBom:
		return Utf16BE_Utf8(aBuffer);
	case CharEncoding::Utf16_LE:
	case CharEncoding::Utf16_LE_NoBom:
		return Utf16BE_Utf16LE(aBuffer);
	default:
		return aBuffer;
	}
}

//-------------------------------------------------------------------------------------------------

std::vector<unsigned char> BufferEncoding::Utf8_Ansi(const std::vector<unsigned char>& aBuffer)
{
	return Utf16LE_Ansi(Utf8_Utf16LE(aBuffer));
}

std::vector<unsigned char> BufferEncoding::Ansi_Utf8(const std::vector<unsigned char>& aBuffer)
{
	return Utf16LE_Utf8(Ansi_Utf16LE(aBuffer));
}

std::vector<unsigned char> BufferEncoding::Utf16LE_Ansi(const std::vector<unsigned char>& aBuffer)
{
	if (aBuffer.empty())
		return{};
	
	int size_needed = WideCharToMultiByte(CP_ACP,
										  0, 
										  reinterpret_cast<wchar_t*>(const_cast<unsigned char*>(aBuffer.data())),
										  aBuffer.size() / sizeof(wchar_t),
										  nullptr,
										  0, 
										  nullptr,
										  nullptr);

	std::vector<unsigned char> buff(size_needed, 0);

	auto res = WideCharToMultiByte(CP_ACP,
								   0,
								   reinterpret_cast<wchar_t*>(const_cast<unsigned char*>(aBuffer.data())),
								   aBuffer.size() / sizeof(wchar_t),
								   reinterpret_cast<char*>(buff.data()),
								   size_needed,
								   nullptr,
								   nullptr);

	return buff;
}

std::vector<unsigned char> BufferEncoding::Ansi_Utf16LE(const std::vector<unsigned char>& aBuffer)
{
	if (aBuffer.empty())
		return{};

	int size_needed = MultiByteToWideChar(CP_ACP,
										  0,
										  reinterpret_cast<char*>(const_cast<unsigned char*>(aBuffer.data())),
										  aBuffer.size(),
										  nullptr,
										  0);

	std::vector<unsigned char> buff(size_needed * sizeof(wchar_t), 0);

	auto res = MultiByteToWideChar(CP_ACP,
								   0,
								   reinterpret_cast<char*>(const_cast<unsigned char*>(aBuffer.data())),
								   aBuffer.size(),
								   reinterpret_cast<wchar_t*>(buff.data()),
								   size_needed);

	return buff;
}

std::vector<unsigned char> BufferEncoding::Utf16BE_Ansi(const std::vector<unsigned char>& aBuffer)
{
	if (aBuffer.empty())
		return{};

	auto buffBE = ChangeEndianness(aBuffer);

	int size_needed = WideCharToMultiByte(CP_ACP,
										  0,
										  reinterpret_cast<wchar_t*>(const_cast<unsigned char*>(buffBE.data())),
										  buffBE.size() / sizeof(wchar_t),
										  nullptr,
										  0,
										  nullptr,
										  nullptr);

	std::vector<unsigned char> buff(size_needed, 0);

	auto res = WideCharToMultiByte(CP_ACP,
								   0,
								   reinterpret_cast<wchar_t*>(const_cast<unsigned char*>(buffBE.data())),
								   buffBE.size() / sizeof(wchar_t),
								   reinterpret_cast<char*>(buff.data()),
								   size_needed,
								   nullptr,
								   nullptr);

	return buff;
}

std::vector<unsigned char> BufferEncoding::Ansi_Utf16BE(const std::vector<unsigned char>& aBuffer)
{	
	return ChangeEndianness(Ansi_Utf16LE(aBuffer));
}

std::vector<unsigned char> BufferEncoding::Utf16LE_Utf8(const std::vector<unsigned char>& aBuffer)
{
	if (aBuffer.empty())
		return{};

	int size_needed = WideCharToMultiByte(CP_UTF8,
										  0, 
										  reinterpret_cast<wchar_t*>(const_cast<unsigned char*>(aBuffer.data())),
										  aBuffer.size() / sizeof(wchar_t),
										  nullptr, 
										  0,
										  nullptr,
										  nullptr);

	std::vector<unsigned char> buff(size_needed, 0);

	auto res = WideCharToMultiByte(CP_UTF8,
								   0, 
								   reinterpret_cast<wchar_t*>(const_cast<unsigned char*>(aBuffer.data())),
								   aBuffer.size() / sizeof(wchar_t),
								   reinterpret_cast<char*>(buff.data()),
								   size_needed,
								   nullptr,
								   nullptr);
	return buff;
}

std::vector<unsigned char> BufferEncoding::Utf8_Utf16LE(const std::vector<unsigned char>& aBuffer)
{
	if (aBuffer.empty())
		return{};

	int size_needed = MultiByteToWideChar(CP_UTF8,
										  0,
										  reinterpret_cast<char*>(const_cast<unsigned char*>(aBuffer.data())),
										  aBuffer.size(),
										  nullptr,
										  0);

	std::vector<unsigned char> buff(size_needed * sizeof(wchar_t), 0);

	auto res = MultiByteToWideChar(CP_UTF8,
								   0,
								   reinterpret_cast<char*>(const_cast<unsigned char*>(aBuffer.data())),
								   aBuffer.size(),
								   reinterpret_cast<wchar_t*>(buff.data()),
								   size_needed);

	return buff;
}

std::vector<unsigned char> BufferEncoding::Utf16BE_Utf8(const std::vector<unsigned char>& aBuffer)
{
	return Utf16LE_Utf8(ChangeEndianness(aBuffer));
}

std::vector<unsigned char> BufferEncoding::Utf8_Utf16BE(const std::vector<unsigned char>& aBuffer)
{
	return ChangeEndianness(Utf8_Utf16LE(aBuffer));
}

std::vector<unsigned char> BufferEncoding::Utf16BE_Utf16LE(const std::vector<unsigned char>& aBuffer)
{
	return ChangeEndianness(aBuffer);
}

std::vector<unsigned char> BufferEncoding::Utf16LE_Utf16BE(const std::vector<unsigned char>& aBuffer)
{
	return ChangeEndianness(aBuffer);
}

std::vector<unsigned char> BufferEncoding::ChangeEndianness(const std::vector<unsigned char>& aBuffer)
{
	auto buff = aBuffer;
	for (int i = 1; i < buff.size(); i += 2)
		std::swap(buff[i - 1], buff[i]);

	return buff;
}
