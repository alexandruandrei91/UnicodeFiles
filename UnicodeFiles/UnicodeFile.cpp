#include "stdafx.h"
#include "UnicodeFile.h"
#include "Utils.h"
#include "Exception.h"

using namespace UnicodeFiles;

UnicodeFile::UnicodeFile()
	: mNoBuffering(false), mAppend(false), mWrite(false)
{
	
}

UnicodeFile::UnicodeFile(std::wstring aFilePath, unsigned int aFlags)
	: mNoBuffering(false), mAppend(false)
{
	Reset(aFilePath, aFlags);
}

UnicodeFile::~UnicodeFile()
{
	Flush();
}

void UnicodeFile::Reset(std::wstring aFilePath, unsigned int aFlags)
{
	//default values
	unsigned long desiredAccess = 0;
	unsigned long creationDisposition = OPEN_EXISTING;
	unsigned long flagsAndAttributes = FILE_ATTRIBUTE_NORMAL;

	if (aFlags & UF_READ)
		desiredAccess |= GENERIC_READ;

	if (aFlags & UF_WRITE)
	{
		desiredAccess |= GENERIC_WRITE;
		mWrite = true;
	}

	if (desiredAccess == 0)
	{
		desiredAccess = GENERIC_READ | GENERIC_WRITE;
		mWrite = true;
	}


	if ((aFlags & UF_CREATE) && (aFlags & UF_TRUNCATE))
		creationDisposition = CREATE_ALWAYS;
	else if (aFlags & UF_CREATE)
			creationDisposition = OPEN_ALWAYS;
	else if (aFlags & UF_TRUNCATE)
			creationDisposition = TRUNCATE_EXISTING;
	
	if (aFlags & UF_NO_BUFFERING)
		mNoBuffering = true;

	if (aFlags & UF_APPEND)
		mAppend = true;

	//use close and set instead of reset to avoid calling CreateFile before closing the handle.
	mFileHandle.Close();
	mFileHandle.Set(::CreateFile(aFilePath.c_str(),
								   desiredAccess,
								   0,
								   nullptr,
								   creationDisposition,
								   flagsAndAttributes,
								   nullptr));
	if (mFileHandle == INVALID_HANDLE_VALUE)
		Utils::CheckLastError();
}

std::vector<unsigned char> UnicodeFile::ReadFile(unsigned int aChunkSize) const
{
	if (aChunkSize == 0)
	{
		aChunkSize = GetFileSize();
	}

	std::vector<unsigned char> buff(aChunkSize);

	unsigned long readCount;

	if (!::ReadFile(mFileHandle, buff.data(), aChunkSize, &readCount, nullptr))
		Utils::CheckLastError();

	buff.resize(readCount);

	return buff;
}

void UnicodeFile::WriteFile(const std::vector<unsigned char>& aBuffer, bool aResetPos)
{
	if (mAppend)
	{
		if (::SetFilePointer(mFileHandle, 0, nullptr, FILE_END) == INVALID_SET_FILE_POINTER)
			Utils::CheckLastError();
	}
	else
	{
		if (::SetFilePointer(mFileHandle, 0, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
			Utils::CheckLastError();
	}

	unsigned long writeCount;
	if (!::WriteFile(mFileHandle, aBuffer.data(), aBuffer.size(), &writeCount, nullptr))
		Utils::CheckLastError();

	if (mNoBuffering)
		Flush();

	if (aResetPos)
	{
		if (::SetFilePointer(mFileHandle, 0, nullptr, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
			Utils::CheckLastError();
	}
}

void UnicodeFile::Flush()
{
	if (mWrite)
	{
		if (!::FlushFileBuffers(mFileHandle))
			Utils::CheckLastError();
	}
}

int UnicodeFile::GetFileSize() const
{
	auto size = ::GetFileSize(mFileHandle, nullptr);
	if (size == INVALID_FILE_SIZE)
		Utils::CheckLastError();

	return size;
}