#pragma once

class Exception;

/**@file*/

/**
*@namespace UnicodeFiles
*It contains all the classes in this project.
*/
namespace UnicodeFiles
{
	/**
	*Utilities Class.
	*
	*A class that contains several static functions for different purposes.
	*/
	class Utils
	{
	public:
		/**
		*Deleted constructor, so the class is not instantiable.
		*/
		Utils() = delete;

		/**
		*Check if the last Win32 call returned encountered an error.
		*
		*@param aThrow [default=true] if aThrow is true and there is an error, the function will throw an exception, otherwise it will return the exception.
		*@return The exception that occurred.
		*@see Exception
		*/
		static Exception CheckLastError(bool aThrow = true);

		/**
		*Convert from a buffer (vector of unsigned chars) to a wstring.
		*
		*@param aBuff The buffer to be converted.
		*@return The resulting wstring.
		*/
		static std::wstring Buff_Wstring(const std::vector<unsigned char>& aBuff);
		/**
		*Convert from a buffer (vector of unsigned chars) to a string.
		*
		*@param aBuff The buffer to be converted.
		*@return The resulting string.
		*/
		static std::string Buff_String(const std::vector<unsigned char>& aBuff);

		/**
		*Convert from a wstring to a buffer (vector of unsigned chars).
		*
		*@param aStr The wstring to be converted.
		*@return The resulting buffer.
		*/
		static std::vector<unsigned char> Wstring_Buff(const std::wstring& aStr);
		/**
		*Convert from a string to a buffer (vector of unsigned chars).
		*
		*@param aStr The string to be converted.
		*@return The resulting buffer.
		*/
		static std::vector<unsigned char> Buff_String(const std::string& aStr);
	};
}